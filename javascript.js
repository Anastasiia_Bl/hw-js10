

document.querySelector('.tab-title').classList.add('active');
document.querySelector('.tab-content').classList.add('active');

function selectContent (e) {
    let target = e.target.dataset.target;
    
    document.querySelectorAll('.tab-title, .tab-content').forEach(el => el.classList.remove('active'));
    e.target.classList.add('active');
    document.querySelector('.' + target).classList.add('active');
}

document.querySelectorAll('.tab-title').forEach(el => {
    el.addEventListener('click', selectContent);
})
